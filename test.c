#include <stdio.h>
#include <unistd.h>
#include "util.h"

void test_create_fifo(void);
void test_create_whole_message(void);

int main() {

	printf("Testing create_fifo()\n");
	test_create_fifo();
	
	printf("Testing create_whole_message\n");
	test_create_whole_message();
	
	return 0;
}

void test_create_fifo() {
	printf("Creating a fifo that does not exist.\n");
	
	create_fifo("testFIFO");
	
	if (access("testFIFO", F_OK) == -1) {
		printf("Test failed. FIFO was not created.\n");
	} else {
		printf("Creating fifo test passed.\n");
	}
	
	printf("Try creating a FIFO even though it exists.\n");
	
	int res;
	res = create_fifo("testFIFO");
	if (res == 0) {
		printf("Test passed. Didn't recreate FIFO.\n");
	} else {
		printf("Test failed. Error encountered creating FIFO with same name.\n");
	}
}

void test_create_whole_message() {
	printf("Combining a handle and a message.\n");
	printf("Handle: eric\n");
	printf("Message 'hello there'\n");
	
	char *whole_message = create_whole_message("eric", "hello there");
	
	printf("Message should be:\n");
	printf("eric: hello there\n");
	printf("Message is:\n %s\n", whole_message);
}
