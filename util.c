#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

/* create_fifo: Check to see if the FIFO exists.
 * 				If it doesn't, create it.
 * */
int create_fifo(char *fifo_name) {
	int res;
	if (access(fifo_name, F_OK) == -1) {
		res = mkfifo(fifo_name, S_IFIFO | 0666);
		if (res != 0)
			// If there is an issue, return -1
			return -1;
	}
	// If there is no problem, return 0
	return 0;
}

/* create_whole_message: Combine the handle and message */
char *create_whole_message(char *handle, char *message) {
	int size = strlen(handle) + strlen(": ") + strlen(message) + 1;
	char *whole_message = (char *) malloc(size);
	strcpy(whole_message, handle);
	strcat(whole_message, ": ");
	strcat(whole_message, message);
	whole_message[size-1] = '\0';
	return whole_message;
}
