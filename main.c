#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include "util.h"

#define MAX_MESSAGE_LEN PIPE_BUF

int main(int argc, char* argv[]) {

	char *handle;
	char *read_fifo;
	char *write_fifo;

	// Get the desired FIFO name and user handle.
	// argv[1] = client
	// argv[2] = handle
	if (argc == 3) {
		if (strcmp(argv[1], "1") == 0) {
			read_fifo = "fifo1";
			write_fifo = "fifo2";
		} else if (strcmp(argv[1], "2") == 0) {
			read_fifo = "fifo2";
			write_fifo = "fifo1";
		} else {
			printf("Client %s not supported.\n", argv[1]);
			return -1;
		}
		handle = argv[2];
	} else {
		printf("Incorrect invocation of chat program.\n");
		return -1;
	}
	
	printf("Welcome to chat.\nYour read FIFO is %s and your write FIFO is %s\n", read_fifo, write_fifo);
	fflush(stdout);
	
	// Create the FIFOs
	if (create_fifo(read_fifo) == -1)
		return -1;
	
	if (create_fifo(write_fifo) == -1)
		return -1;
	
	// Create the processes to handle input/output to file.
	int pid = fork();
	
	// Sets the acceptable message size to the MAX, less
	//	the length of the username and space for ": ", so
	//	that it can be fully written to the FIFO.
	char message[MAX_MESSAGE_LEN - (strlen(handle) + 2)];

	// READ process
	if(pid > 0) {
	
		int fd, num;
		
		// Open the FIFO
		fd = open(read_fifo, O_RDONLY);
		
		// Read from the FIFO
		while (1) {
			if ((num = read(fd, message, MAX_MESSAGE_LEN)) == -1) {
				perror("read");
			} else {
				message[num] = '\0';
				printf("%s\n", message);
				fflush(stdout);
			}
		}
		
	// WRITE process
	} else if(pid == 0) {	
	
		int fd, num;
		
		// Open the FIFO
		fd = open(write_fifo, O_WRONLY);
	
		// Get input from the user
		while (gets(message), !feof(stdin)) {
			
			// Combine the handle and message to write
			char *whole_message = create_whole_message(handle, message);
			
			// Write the message to the FIFO
			if ((num = write(fd, whole_message, strlen(whole_message))) == -1) {
				perror("write");
			}
		}
		
	} else {
		printf("fork failed!\n");
	}

	return 0;
}
