CC=/usr/bin/gcc
CFLAGS= -g -Wall -I.
LINKFLAGS= -g -Wall

all: chat test

chat: main.o util.o
	$(CC) $(LINKFLAGS) -o chat main.o util.o

test: test.o util.o
	$(CC) $(LINKFLAGS) -o test test.o util.o

main.o: main.c util.h
	$(CC) $(CFLAGS) -c main.c -o main.o

test.o: test.c util.h
	$(CC) $(CFLAGS) -c test.c -o test.o

clean:
	rm -f *.o chat test
